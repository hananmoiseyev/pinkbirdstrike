﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Background : MonoBehaviour
{
    float topY;
    int niceScoreSize = 4;
    string niceScore = "";
    private Vector3 PinkBirdCurrentPos;

    [SerializeField]
    private float jumpForce = 400f;
    private Rigidbody2D rb;

    private void Awake()
    {
        rb = GameObject.Find("PinkBird").GetComponent<Rigidbody2D>();
    }


    // Start is called before the first frame update
    void Start()
    {
        topY = 0;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetButtonDown("Fire1") || Input.GetKeyDown("space"))
        {
            // Bump the bird
            rb.velocity = Vector2.zero;
            rb.AddForce(Vector2.up * (jumpForce - PinkBirdCurrentPos.y));

            // Flap those wings!
            GameObject.Find("PinkBird").GetComponent<Animator>().enabled = true;
            GameObject.Find("PinkBird").GetComponent<Animator>().Play("PinkBird");
            
        }

        // Score
        PinkBirdCurrentPos =  GameObject.Find("PinkBird").transform.position;
        if (topY < PinkBirdCurrentPos.y && PinkBirdCurrentPos.y > 0)
        {
            topY = PinkBirdCurrentPos.y;
            GameObject.Find("Triangle0").transform.position = new Vector3(GameObject.Find("Triangle0").transform.position.x, topY);
            niceScore = Mathf.Round(topY).ToString();
            while (niceScore.Length < niceScoreSize)
            {   
                niceScore = "0" + niceScore;
            }
            GameObject.Find("Score").GetComponent<TextMesh>().text = niceScore;
        }
        
        // Slowly make the bird fly straight
        Quaternion PinkBirdCurRotation = GameObject.Find("PinkBird").transform.rotation;
        //print(Mathf.Abs(PinkBirdCurRotation.z).ToString());
        if (PinkBirdCurRotation.z != 0 && PinkBirdCurrentPos.y > 0)
        {
            //If we are over 180, rotate down, otherwise rotate up
            float rotation = ((float)(0.1));
            if (Mathf.Abs(PinkBirdCurRotation.z) > 0)
            {
                rotation = rotation * -1;
            }
            GameObject.Find("PinkBird").transform.rotation = new Quaternion(PinkBirdCurRotation.x, PinkBirdCurRotation.y, Mathf.Abs(PinkBirdCurRotation.z) + rotation,PinkBirdCurRotation.w);
        }

        
        

        foreach(Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                //OnMouseDown();
            }
        }

        // Follow the bird with the camera
        PinkBirdCurrentPos =  GameObject.Find("PinkBird").transform.position;
        Vector3 MainCameraPosition =  GameObject.Find("MainCamera").transform.position;
        if (MainCameraPosition.y < 0)
        {
            // Do nothing
            GameObject.Find("MainCamera").transform.position = new Vector3(0, 0, -10);
            GameObject.Find("Score").transform.position = new Vector3(((float)0), ((float)5) ,10);
        }
        else if (PinkBirdCurrentPos.y - MainCameraPosition.y > 3)  
        {
            GameObject.Find("MainCamera").transform.position = new Vector3(0, PinkBirdCurrentPos.y - 3, -10);
            GameObject.Find("Score").transform.position = new Vector3(((float)0), (PinkBirdCurrentPos.y - 3 + (float)5) ,10);
        }
        else if (PinkBirdCurrentPos.y - MainCameraPosition.y < -3)
        {
            GameObject.Find("MainCamera").transform.position = new Vector3(0, PinkBirdCurrentPos.y + 3, -10);
            GameObject.Find("Score").transform.position = new Vector3(((float)0), (PinkBirdCurrentPos.y + 3 + (float)5) ,10);
        }
        

    }

 
}
