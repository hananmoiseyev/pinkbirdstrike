﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }



    void OnCollisionEnter2D(Collision2D col)
    {
        // Stop those wings!
        //print("bang!");
        GameObject.Find("PinkBird").GetComponent<Animator>().enabled = false;
        MoveLeft.backgroundSpeed = 0;
    }

    void OnCollisionExit2D(Collision2D col)
    {
        MoveLeft.backgroundSpeed = 2;
    }
}
