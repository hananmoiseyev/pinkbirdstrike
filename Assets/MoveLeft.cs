﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveLeft : MonoBehaviour
{
    [SerializeField]
    public static float backgroundSpeed = 1f;

        

    // Update is called once per frame
    void Update()
    {
      

        transform.position += Time.deltaTime * backgroundSpeed * Vector3.left;
        if (transform.position.x < -13)
        {
            transform.position = new Vector3(0,transform.position.y, transform.position.z);
        }
         
    }
}
